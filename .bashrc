
# If not running interactively, don't do anything
[[ $- == *i* ]] || return

source ~/.dotfiles/shrc

# History
HISTCONTROL=ignorespace    # ignore lines starting with a space
HISTSIZE=100000            # history line count
shopt -s histappend        # append to the history file, don't overwrite it

# Check the window size after each command and, if necessary, update
# the values of LINES and COLUMNS.
shopt -s checkwinsize

# Enable auto-completion
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# Eye candy prompt
source ~/.dotfiles/colors.bash
if [[ $(id -u) = 0 ]]; then     # root
    PS1="\[$Red\].:\[$BRed\]\u\[$BBlue\]@\[$BGreen\]\h\[$BYellow\](bash)\[$Red\]:. \[$BBlue\][\w]\[$BRed\n#\[$Color_Off\] "
    PS2="\[$BRed\]>\[$Color_Off\] "
else
    PS1="\[$Red\].:\[$BGreen\]\u\[$BBlue\]@\[$BGreen\]\h\[$BYellow\](bash)\[$Red\]:. \[$BBlue\][\w]\[$BGreen\n\$\[$Color_Off\] "
    PS2="\[$BGreen\]>\[$Color_Off\] "
fi

if [ -f ~/.local.bash ]; then
    source ~/.local.bash
fi
