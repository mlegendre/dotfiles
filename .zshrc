
# If not running interactively, don't do anything
[[ $- == *i* ]] || return

# Enable auto-completion
autoload -Uz compinit
compinit
autoload -U bashcompinit
bashcompinit

# Restrict auto-completion for `make` to targets
zstyle ':completion:*:*:make:*' tag-order 'targets'

# Enable fzf shell extensions
if [ -e /usr/share/doc/fzf/examples/key-bindings.zsh ]; then
   source /usr/share/doc/fzf/examples/key-bindings.zsh
fi
if [ -e /usr/share/doc/fzf/examples/completion.zsh ]; then
   source /usr/share/doc/fzf/examples/completion.zsh
fi

source ~/.dotfiles/shrc

# History
HISTFILE=~/.histfile        # Location of history
HISTSIZE=100000             # Number of lines kept in history
SAVEHIST=100000             # Number of lines saved in the history after logout
setopt hist_ignore_space    # Do not remember lines beginning with a space
setopt appendhistory        # Don't lose history added by another instance

# Prompt
userId=$(id -u)
if [[ "$(tput colors)" -ge 8 ]]; then # colors supported
    cDots="%b%F{red}"
    cAt="%B%F{blue}"
    cHost="%B%F{green}"
    cDir="%B%F{blue}"
    cEnd="%b%f"
    if [[ $userId = 0 ]]; then # root
        cUser="%B%F{red}"
        cPrompt="%B%F{red}"
    else
        cUser="%B%F{green}"
        cPrompt="%B%F{green}"
    fi
else # colors not supported
    cDots= cUser= cAt= cHost= cDir= cPrompt= cEnd=
fi
[ $userId = 0 ] && prompt="#" || prompt="$" # Prompt symbol
export PS1=$cDots'.:'$cUser'%n'$cAt'@'$cHost'%'M$cDots':. '${cDir}'[%~]
'$cPrompt"$prompt "$cEnd
export PS2=$cPrompt"> "$cEnd

# Key bindings
bindkey -e  # Choose Emacs keymap
bindkey "\ei" up-line-or-history
bindkey "\ek" down-line-or-history
bindkey "\ej" backward-char
bindkey "\el" forward-char
bindkey "\eJ" backward-word
bindkey "\eL" forward-word
bindkey "\eu" beginning-of-line
bindkey "\eo" end-of-line
bindkey "\em" undo
bindkey "\e " set-mark-command

# Movement widget with '/' as a separator
shell-backward-delete-word () {
    local WORDCHARS="${WORDCHARS/\/}"
    zle backward-delete-word
}
zle -N shell-backward-delete-word

shell-backward-word () {
    # Consider '/' a separator
    local WORDCHARS="${WORDCHARS/\/}"
    zle backward-word
}
zle -N shell-backward-word

shell-forward-word () {
    # Consider '/' a separator
    local WORDCHARS="${WORDCHARS/\/}"
    zle forward-word
}
zle -N shell-forward-word

# Alt-Backspace
bindkey    '\e^?' shell-backward-delete-word
# Alt-Left
bindkey    '^[[1;3D' shell-backward-word
# Alt-Right
bindkey    '^[[1;3C' shell-forward-word

# Edit the current command line in $EDITOR
autoload -U edit-command-line
zle -N edit-command-line
bindkey '\C-x\C-e' edit-command-line

if [ -f ~/.local.zsh ]; then
    source ~/.local.zsh
fi
