(setq custom-file (concat user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

(add-to-list 'load-path "~/.dotfiles/emacs")

;; Enable a few minor modes
(global-font-lock-mode t)       ; Enable Font Lock (syntax highlighting)
(transient-mark-mode t)         ; Highlight region between mark and cursor
(line-number-mode t)            ; Show line number in the mode line
(column-number-mode t)          ; Show column number in the mode line
(global-auto-revert-mode 1)     ; Revert a buffer when the file on disk changes
(show-paren-mode t)             ; Highlight matching parentheses

;; Re-enable disabled functions
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(load "keybindings.el")
(load "functions.el")

;; Ask "y" or "n" instead of "yes" or "no"
(fset 'yes-or-no-p 'y-or-n-p)

;; ;; https://www.masteringemacs.org/article/introduction-to-ido-mode
;; (setq ido-enable-flex-matching t)
;; (setq ido-everywhere t)
;; (ido-mode 1)

;; Backups
(setq
 ; Don't scatter backup files everywhere
 backup-directory-alist `(("." . "~/.saves"))
 ; Keep multiple backups
 version-control t
 delete-old-versions t
 kept-old-versions 5
 kept-new-versions 5)

;; Configure default values for some variables
(setq-default
 user-full-name "Marc Legendre"
 user-mail-address "marc.legendre.89@gmail.com"
 initial-scratch-message ";; Scratch buffer\n\n"
 inhibit-startup-screen t
 nxml-child-indent 4                    ; XML indentation
 sgml-basic-offset 4                    ; HTML indentation
 cmake-tab-width 4
 c-basic-offset 4
 tab-width 4
 indent-tabs-mode nil
 fill-column 79
 show-trailing-whitespace t
 highlight-tabs t
 compilation-scroll-output 'first-error
)

(set-face-attribute 'trailing-whitespace nil
                    :background "blanched almond")

;; Set locale to UTF8
(set-language-environment 'utf-8)
(set-terminal-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; French fill style: don't break before '?', '!', etc.
(add-to-list 'fill-nobreak-predicate 'fill-french-nobreak-p)

;; Add bullets (-,+,*) as delimeters for the start of a paragraph.
;; (Default value: "\f\\|[ ]*$")
(setq paragraph-start "\f\\|[ \t]*$\\|[ \t]*[-+*] ")

(unless (= (user-uid) 0)
  (load "packages.el"))

;; Support ANSI colors in compilation-mode
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (ansi-color-apply-on-region compilation-filter-start (point)))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

(when (file-exists-p "~/.local.el")
  (load "~/.local.el"))
