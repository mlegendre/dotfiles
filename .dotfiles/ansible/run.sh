#!/bin/sh
set -ex

which ansible-playbook
sudo --validate
ansible-playbook -i localhost, playbook.yml
