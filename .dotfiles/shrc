
# Configuration common to bash and zsh

export EDITOR='emacs -nw'
export VISUAL='emacs -nw'
export PATH="$PATH:$HOME/.dotfiles/bin:$HOME/bin"

alias cp='cp -i'
alias dfl='df --print-type --human-readable -x tmpfs -x squashfs -x devtmpfs'
alias du='du --human-readable'
alias emacs='emacs --no-window-system'
alias grep='grep --color=auto'
alias la='ls --almost-all'
alias ll='ls -l --human-readable'
alias ls='ls --color=auto'
alias mv='mv -i'
alias o='xdg-open'
alias rm='rm -i'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias .......="cd ../../../../../.."

function mkcd() {
    mkdir -vp "$1" && cd "$1"
}

function e() {
    if [ $# -eq 0 ]; then
        emacs .
    else
        emacs $*
    fi
}

function yaml_validate {
  python -c 'import sys, yaml, json; yaml.safe_load(sys.stdin.read())'
}

function yaml2json {
  python -c 'import sys, yaml, json; print(json.dumps(yaml.safe_load(sys.stdin.read())))'
}

function yaml2json_pretty {
  python -c 'import sys, yaml, json; print(json.dumps(yaml.safe_load(sys.stdin.read()), indent=2, sort_keys=False))'
}

function json_validate {
  python -c 'import sys, yaml, json; json.loads(sys.stdin.read())'
}

function json2yaml {
  python -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read())))'
}

function annotate-output() {
    "$@" 2> >(sed 's/^/stderr: /') > >(sed 's/^/stdout: /')
}

if [ -f ~/.local.sh ]; then
    source ~/.local.sh
fi

# Local Variables:
# mode: sh
# End:
