
(defun filename-matches (filename)
  "Returns nil unless current buffer is a file and its path (i.e.
buffer-file-name) matches the argument"
  (and buffer-file-name
	   (string-match filename buffer-file-name)))

(defun sort-lines-nocase ()
  "Sort lines in region alphabetically, and case-insensitively"
  (interactive)
  (let ((sort-fold-case t))
    (call-interactively 'sort-lines)))

(defun compile-in-directory (dir)
  "Run the `compile' command, but in a given directory."
  (interactive "DCompile directory: ")
  (message "a" dir "b")
  (let ((default-directory dir))
    (command-execute 'compile)))

(defun compile-dwim (prefix)
  (interactive "P")
  (if prefix
      (command-execute 'compile-in-directory)
    (if (boundp 'compile-directory)
        (let ((default-directory compile-directory))
          (recompile)))
      (recompile)))
