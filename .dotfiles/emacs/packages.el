
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(when (< emacs-major-version 27)
  (package-initialize))

;; Bootstrapping: install use-package for easier package setup
;; https://github.com/jwiegley/use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package helm
  :ensure t)

(use-package helm-projectile
  :ensure t)

(use-package projectile
  :ensure t
  :bind-keymap
  ("C-c p" . projectile-command-map)
  ("s-p" . projectile-command-map)
  :init
  (projectile-global-mode)
  (helm-projectile-on))

(setq projectile-use-git-grep t)

(use-package magit
  :ensure t
  :custom
  ;; Enable --set-upstream by default in the push popup
  (magit-push-arguments (quote ("--set-upstream")))
  :bind ("C-c g" . 'magit-status))

(use-package elpy
  :ensure t
  :init
  (elpy-enable))

(use-package markdown-mode
  :ensure t)

(use-package docker
  :ensure t
  :bind ("C-c d" . docker))

(use-package solarized-theme
  :ensure t
  :init
  (when (display-graphic-p)
    (load-theme 'solarized-light)))

;; Detect "path/to/file: warning|error:" in compilation buffer.
;; (builtin values only recognize "path/to/file:lineno:")
(add-to-list 'compilation-error-regexp-alist 'error-with-no-line-number)
(add-to-list 'compilation-error-regexp-alist-alist
             '(error-with-no-line-number "^\\([^ 
:]+\\): \\(error\\|warning\\)" 1 nil nil 2 1))



(setq c-default-style "stroustrup"
      c-basic-offset 4)

;; Use c++-mode for .inl files
(add-to-list 'auto-mode-alist '("\\.inl" . c++-mode))

;; C/C++ mode hook
(add-hook
 'c-mode-common-hook
 (lambda ()
   (set-fill-column 80)
   (local-set-key (kbd "C-c o") 'ff-find-other-file)
   (local-set-key (kbd "C-M-u") 'c-beginning-of-defun)
   (local-set-key (kbd "C-M-o") 'c-end-of-defun)))
