
;; Note to self: in ASCII terminal emulators, only characters between 64 and
;; 126 will have a corresponding control character.
;; Thus, as a rule of thumb, one should use only C-letter chords.
;; Also:
;; - a letter and its uppercase counterpart have the same control character
;; - C-m is RET, C-[ is ESC and C-i is TAB
;; - M-O is ESC O, which is an escape sequence (E.g. <left> is ESC O A)


;;; Cursor navigation
;; Horizontal
(global-set-key (kbd "M-l") 'forward-char)
(global-set-key (kbd "M-j") 'backward-char)
(global-set-key (kbd "M-L") 'forward-word)
(global-set-key (kbd "M-J") 'backward-word)
(global-set-key (kbd "M-u") 'move-beginning-of-line)
(global-set-key (kbd "M-o") 'move-end-of-line)
(global-set-key (kbd "M-U") 'back-to-indentation)
;; Vertical
(global-set-key (kbd "M-k") 'next-line)
(global-set-key (kbd "M-i") 'previous-line)
(global-set-key (kbd "M-K") '(lambda () (interactive) (next-line 10)))
(global-set-key (kbd "M-I") '(lambda () (interactive) (previous-line 10)))
;; Search
(global-set-key (kbd "M-h") 'isearch-forward)
(global-set-key (kbd "M-H") 'isearch-backward)
(define-key isearch-mode-map (kbd "M-h") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "M-H") 'isearch-repeat-backward)

;;; function keys
(global-set-key (kbd "<f8>") 'delete-trailing-whitespace)

;;; Misc
(global-set-key (kbd "C-S-c") 'clipboard-kill-ring-save)
(global-set-key (kbd "C-S-v") 'clipboard-yank)
(global-set-key (kbd "M-m") 'other-window)
(global-set-key (kbd "C-o") 'find-file)
(global-set-key (kbd "C-c o") 'find-file-at-point)
(global-set-key (kbd "C-c C-o") 'org-open-at-point-global)
(global-set-key (kbd "<f9>") 'org-open-at-point-global)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-z") 'kill-region)
(global-set-key (kbd "M-z") 'kill-ring-save)
(global-set-key (kbd "C-b") (lambda () (interactive) (kill-buffer nil)))
(global-set-key (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "M-SPC") 'set-mark-command)
(global-set-key (kbd "C-SPC") 'just-one-space)
(global-set-key (kbd "C-@") 'just-one-space)
(global-set-key (kbd "C-u") 'undo)
(global-set-key (kbd "M-M") 'dabbrev-expand)
(global-set-key (kbd "C-n") 'universal-argument)
(global-set-key (kbd "M-n") 'keyboard-quit)
(define-key minibuffer-local-map (kbd "M-n") 'abort-recursive-edit)
(global-set-key (kbd "M-'") 'query-replace)
(global-set-key (kbd "M-\"") 'query-replace-regexp)
(global-set-key (kbd "M-c") 'completion-at-point)
(global-set-key (kbd "<f12>") 'compile-dwim)

;;; Re-redefine important keybindings redefined by specific modes
;; diff-mode
(add-hook
 'diff-mode-hook
 (lambda ()
   (local-set-key (kbd "M-SPC") 'set-mark-command)
   (local-set-key (kbd "M-k") 'next-line)
   (local-set-key (kbd "M-K") 'forward-paragraph)))
;; nxml-mode
(add-hook
 'nxml-mode-hook
 (lambda ()
   (local-set-key (kbd "M-h") 'isearch-forward)))
(add-hook
 'helm-after-initialize-hook
 (lambda ()
   (define-key helm-projectile-find-file-map (kbd "M-i") 'helm-previous-line) ; was helm-ff-properties-persistent
   (define-key helm-projectile-find-file-map (kbd "M-k") 'helm-next-line)))
;; magit-mode
(add-hook
 'magit-mode-hook
 (lambda ()
   (local-set-key (kbd "M-I") 'magit-section-backward)
   (local-set-key (kbd "M-K") 'magit-section-forward)))

(add-hook
 'elpy-mode-hook
 (lambda ()
   (local-set-key (kbd "<f7>") 'elpy-black-fix-code)))


(defun dired-open-file ()
  "In dired, open the file named on this line."
  (interactive)
  (let* ((file (dired-get-filename nil t)))
    (call-process "xdg-open" nil 0 nil file)))

(add-hook
 'dired-mode-hook
 (lambda ()
   (define-key dired-mode-map (kbd "M-RET") 'dired-open-file)
   (define-key dired-mode-map (kbd "C-c o") 'dired-open-file)))
