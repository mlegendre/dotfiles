
## Setup

### SSH

```
cd
git init
git remote add origin git@gitlab.com:mlegendre/dotfiles.git
git fetch
git reset origin/master
```

### HTTPS

```
cd
git init
git remote add origin https://gitlab.com/mlegendre/dotfiles.git
git fetch
git reset origin/master
```

## Notes

### tmux-colors-solarized

Added with `git-subtree`:

```sh
git subtree add --squash --prefix .dotfiles/tmux-colors-solarized https://github.com/seebi/tmux-colors-solarized.git master
```

Update:

```sh
git subtree pull --squash --prefix .dotfiles/tmux-colors-solarized https://github.com/seebi/tmux-colors-solarized.git master
```
