
### Colors
source-file ~/.dotfiles/tmux-colors-solarized/tmuxcolors-light.conf
set -g default-terminal "xterm-256color"

# Enable mouse support
# note: press shift to disable, if needed
set -g mouse on

# Keep a useful amount of history
set -g history-limit 100000

### Keybindings
## Moving

# Up

bind -T prefix       M-i send-keys -X  history-up
bind -T copy-mode    M-i send-keys -X  cursor-up
bind -T copy-mode    M-, send-keys -X  page-up
bind M-i send-keys -X  up

# Down
bind -T prefix       M-k send-keys -X  history-down
bind -T copy-mode    M-k send-keys -X  cursor-down
bind -T copy-mode    M-. send-keys -X  page-down

# Left
bind -T copy-mode    M-j send-keys -X  cursor-left
bind -T prefix       M-j send-keys -X  cursor-left
bind -T copy-mode    M-J send-keys -X  previous-word
bind -T prefix       M-J send-keys -X  previous-word
bind -T copy-mode    M-u send-keys -X  start-of-line
bind -T prefix       M-u send-keys -X  start-of-line

# Right
bind -T copy-mode    M-l send-keys -X  cursor-right
bind -T prefix       M-l send-keys -X  cursor-right
bind -T copy-mode    M-L send-keys -X  next-word-end
bind -T prefix       M-L send-keys -X  next-word-end
bind -T copy-mode    M-o send-keys -X  end-of-line
bind -T prefix       M-o send-keys -X  end-of-line

# less-like keybindings
bind -T copy-mode    g   send-keys -X  history-top
bind -T copy-mode    G   send-keys -X  history-bottom
bind -T copy-mode    k   send-keys -X  scroll-up
bind -T copy-mode    j   send-keys -X  scroll-down

## Misc
bind -T copy-mode    M-h send-keys -X  search-forward
bind -T copy-mode    M-H send-keys -X  search-backward
bind -T copy-mode    M-n send-keys -X  cancel
bind                  M-n send-keys -X  cancel
bind -T copy-mode    M-Space send-keys -X  begin-selection
# Copy to the system clipboard as well (not only tmux buffers)
# note for the future: tmux 3.2 introduced an option called copy-command to set a command to pipe to for all key bindings.
bind -T copy-mode    M-z send-keys -X  copy-pipe-and-cancel "wl-copy"
bind -T copy-mode MouseDragEnd1Pane send -X copy-pipe-and-cancel 'wl-copy'

bind -T copy-mode    M-I send-keys -X  top-line
bind -T copy-mode    M-K send-keys -X  bottom-line
bind -T prefix       j   copy-mode
bind -T prefix       y   paste-buffer
bind -T prefix       C-y paste-buffer -d
bind -T prefix       v   split-window -h -c "#{pane_current_path}"
bind -T prefix       h   split-window -c "#{pane_current_path}"
bind -T prefix       o   select-pane -t :.+
bind -T prefix       c   new-window -c "#{pane_current_path}"
# Easier when going through multiple panes
bind -T prefix       C-o select-pane -t :.+
bind -T prefix       C-r source-file ~/.tmux.conf
bind -T prefix       m   command-prompt "move-window -t '%%'"
bind -T prefix       r   command-prompt -I "#W" "rename-window '%%'"

## Change prefix
set -g prefix C-j
bind C-j send-prefix
unbind-key C-b


### Start numbering at 1
set -g base-index 1


# # Workaround: when set-clipboard in on, copy-pasting can cause weird characters
# # to be displayed if the wrong TERM value is set
# set -g set-clipboard off
